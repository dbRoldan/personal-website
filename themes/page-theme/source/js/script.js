// Handle NavBar
const navegationBar = document.getElementById('navbar');
const listItems = document.querySelectorAll('.nav-list-link'); //document.getElementsByClassName("nav-list-link");
window.onscroll =  () => {
    "use strict";

    let currentClass = document.documentElement.scrollTop >= 115 ? "nav-colored" : "nav-transparent";
    let removedClass = currentClass == "nav-colored" ? "nav-transparent" : "nav-colored";
    navegationBar.classList.add(currentClass);
    navegationBar.classList.remove(removedClass);

    for (let i = 0; i < listItems.length; i++) {
      if (currentClass == "nav-colored")
        listItems[i].style.color = "snow";
        else
          listItems[i].style.color = "white";
    }
};

// Change theme
window.onload = () => {
    let theme = localStorage.theme;
    loadTheme(theme);
}


function getEventType(event) {
    let theme = localStorage.theme;
    if (event.type) {
        loadTheme(theme);
    }
}
document.addEventListener('click', getEventType, false);

function loadTheme(theme){
    if (theme == 'dark') {
        let x = document.getElementById('messageMail');
        let y = document.getElementById('subjectMail');
        let z = document.getElementById('fromMail');
        x.className = 'dark';
        y.className = 'dark';
        z.className = 'dark';
    } else {
        let x = document.getElementById('messageMail');
        let y = document.getElementById('subjectMail');
        let z = document.getElementById('fromMail');
        x.className = 'light';
        y.className = 'light';
        z.className = 'light';
    }
    document.documentElement.setAttribute("data-theme", theme);
}
