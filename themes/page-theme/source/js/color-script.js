document.getElementById('change').addEventListener('click', () => {
    let currentTheme = localStorage.theme;
    let useTheme = 'light';
    if (currentTheme == 'dark' || !currentTheme) {
        useTheme = 'light';
    } else {
        useTheme = 'dark';
    }
    localStorage.setItem('theme', useTheme);
  });
