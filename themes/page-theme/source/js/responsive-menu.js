// Menu Responsive
function useMenuResponsive() {
  let navegationBar = document.getElementById('navbar');
  let listClass = navegationBar.classList;
  for (var i = 0; i < listClass.length; i++) {
    if (listClass[i] == 'nav'){
      currentType = 'responsive';
      lastType = 'nav';
    } else if (listClass[i] == 'responsive') {
      currentType = 'nav';
      lastType = 'responsive';
    }
  }
  navegationBar.classList.add(currentType);
  navegationBar.classList.remove(lastType);
}
